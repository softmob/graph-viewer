#include <QApplication>
#include <QtWidgets>
#include <QPainter>
#include <QTextCodec>
#include <QString>
#include <sstream>
#include <fstream>
#include <vector>
#include <set>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <QtGui>

double distance(int x1, int y1, int x2, int y2)
{
    return sqrt((x1 - x2) * (x1 - x2) +
                (y1 - y2) * (y1 - y2));
}

std::pair<int,int> pruning(int x0, int y0, int x1, int y1, int r)
{
    double x = x1 - x0;
    double y = y1 - y0;
    double a = atan(y / x);
    x = x0 + r * cos(a);
    y = y0 + r * sin(a);
    if (distance(x, y, x1, y1) > distance(x0, y0, x1, y1))
    {
        x = x0 - r * cos(a);
        y = y0 - r * sin(a);
    }
    return std::make_pair(x, y);
}

class graph
{
public:
    graph()
    {}

    graph(const QString& rhs)
    {
        std::stringstream in(rhs.toStdString());
        size_t n;
        in >> n;
        v.resize(n, std::vector<int> (n));
        for (size_t i = 0; i < n; ++i)
        {
            for (size_t j = 0; j < n; ++j)
            {
                in >> v[i][j];
            }
        }
    }

    graph(size_t n)
    {
        v.resize(n, std::vector<int> (n));
        for (size_t i = 0; i < n; ++i)
        {
            for (size_t j = 0; j < n; ++j)
            {
                int link = rand() % 100;
                if (link > 85 && i != j)
                    v[i][j] = 1;
            }
        }
    }

    graph(const std::vector<std::vector<int> >& rhs): v(rhs)
    {}

    void coordinates(bool f = false)
    {
        srand(time(0));
        double pi = acos(-1);
        q.resize(v.size());
        std::set<std::pair<double, double> > different;
        for (size_t i = 0; i != q.size(); ++i)
        {
            if (f == true)
            {
                q[i].first = m_size / 2 + (m_size / exp(1)) * cos(2 * pi * i / q.size());
                q[i].second = m_size / 2 + (m_size / exp(1)) * sin(2 * pi * i / q.size());
            }
            else
            {
                do
                {
                    q[i].first = rand() % (m_size / 2) + m_size / 4;
                    q[i].second = rand() % (m_size / 2) + m_size / 4;
                }
                while (different.find(q[i]) != different.end());
                different.insert(q[i]);
            }
        }
        laying();
    }

    void laying()
    {
        int counter = 0;
        double eps = .5;
        double SpringConst = -0.05, ElecConst = 50, Grav = 5;
        std::vector<std::pair<double, double> > fxy(q.size());

        while (true)
        {
            std::pair<double, double> sum, dxy;
            for (size_t i = 0; i < v.size(); ++i)
            {
                std::pair<double, double> f;
                for (size_t j = 0; j < v[i].size(); ++j)
                {
                    if (i == j)
                        continue;
                    double dist = distance(q[i].first, q[i].second,
                                           q[j].first, q[j].second);
                    dxy.first = q[i].first - q[j].first;
                    dxy.second = q[i].second - q[j].second;

                    f.first += ElecConst / (dist * dist) * dxy.first / dist;
                    f.second += ElecConst / (dist * dist) * dxy.second / dist;

                    if (v[i][j] != 0)
                    {
                        f.first += SpringConst * dxy.first / dist;
                        f.second += SpringConst * dxy.second / dist;
                    }
                }

                fxy[i] = f;
                sum.first += fabs(f.first);
                sum.second += fabs(f.second);
            }

            for (size_t i = 0; i < q.size(); ++i)
            {
                std::pair<double, double> vij(m_size / 2 - q[i].first, m_size / 2 - q[i].second);
                double dist = distance(vij.first, vij.second, q[i].first, q[i].second);

                fxy[i].first += vij.first * Grav / dist / dist;
                fxy[i].second += vij.second * Grav / dist / dist;

                double k = 1e2;

                q[i].first += fxy[i].first / k;
                q[i].second += fxy[i].second / k;
            }

            if (sqrt(sum.first * sum.first + sum.second * sum.second ) < eps || ++counter > 10000)
                break;
        }
    }

    int get_size()
    {
        return m_size;
    }

    static const int m_size = 100;
    std::vector<std::vector<int> > v;
    std::vector<std::pair<double, double> > q;
};

class drawing : public QWidget
{
public:
    drawing (QWidget *parent = 0) : QWidget(parent)
    {
    }

    void setGraph(graph *rhs)
    {
        mag = rhs;
    }

    ~drawing()
    {
        delete mag;
    }
protected:
    void paintEvent(QPaintEvent *)
    {
        QPainter p(this);

        std::vector<std::vector<int> >& v = mag->v;
        std::vector<std::pair<double, double> > q = mag->q;

        combination(q);

        p.setRenderHint(QPainter::Antialiasing, true);
        p.setRenderHint(QPainter::TextAntialiasing, true);
        p.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
        p.setBrush(QBrush(Qt::cyan, Qt::SolidPattern));

        int min_size = std::min(size().height(), size().width());
        p.setFont(QFont("Times", min_size / 35 ));

        int radius = min_size / 25;

        for (size_t i = 0; i < v.size(); ++i)
        {
            for (size_t j = 0; j < v[i].size(); ++j)
            {
                if (v[i][j] == 0)
                    continue;

                std::pair<int, int> a, b;
                a = pruning(q[i].first, q[i].second, q[j].first, q[j].second, radius);
                b = pruning(q[j].first, q[j].second, q[i].first, q[i].second, radius);

                if (!(v[i][j] != v[j][i] || std::make_pair(i, j) < std::make_pair(j, i)))
                    continue;

                p.drawLine(a.first, a.second, b.first, b.second);

                if (v[i][j])
                {
                    double u = atan2(a.first-b.first, a.second - b.second), len = 10, alfa = 3.14/10;

                    p.drawLine(b.first, b.second, b.first + len * sin(u + alfa), b.second + len * cos(u + alfa));

                    p.drawLine(b.first, b.second, b.first + len * sin(u - alfa), b.second + len * cos(u - alfa));
                }

                if (v[j][i])
                {
                    double u = atan2(-a.first+b.first, -a.second + b.second), len = 10, alfa = 3.14/10;
                    p.drawLine(a.first, a.second, a.first + len * sin(u + alfa), a.second + len * cos(u + alfa));
                    p.drawLine(a.first, a.second, a.first + len * sin(u - alfa), a.second + len * cos(u - alfa));
                }
            }
        }

        for (size_t i = 0; i < q.size(); ++i)
        {
            p.drawEllipse(q[i].first - radius, q[i].second - radius, 2 * radius, 2 * radius);
            QString vertices;
            vertices.setNum(i + 1);

            int dig = log10(i+1)+1;
            dig -= dig / 2;
            p.drawText(q[i].first - dig * p.font().pointSize() / 2,
                       q[i].second + p.font().pointSize() / 2, vertices);
        }
    }
private:
    void combination(std::vector<std::pair<double, double> >& q)
    {
        std::pair<double, double> point_a(q.front()), point_b(q.front());
        for (size_t i = 0; i < q.size(); ++i)
        {
            if (q[i].first < point_a.first)
                point_a.first = q[i].first;
            if (q[i].second < point_a.second)
                point_a.second = q[i].second;
            if (q[i].first > point_b.first)
                point_b.first = q[i].first;
            if (q[i].second > point_b.second)
                point_b.second = q[i].second;
        }

        double m_max_size = std::max(point_b.first - point_a.first, point_b.second - point_a.second);
        int dest_min = std::min(size().height(), size().width());
        double ratio =  dest_min / m_max_size;
        double percent = 1 - 0.8;
        ratio -= ratio * percent;
        for (size_t i = 0; i < q.size(); ++i)
        {
            q[i].first -= point_a.first;
            q[i].second -= point_a.second;
            q[i].first *= ratio;
            q[i].second *= ratio;
            q[i].first += (size().width() - (point_b.first - point_a.first) * ratio) / 2 ;
            q[i].second += (size().height() - (point_b.second - point_a.second) * ratio) / 2;
        }
    }

    graph *mag;
};

class myMainWidget : public QMainWindow
{
    Q_OBJECT
private slots:
    void newfile()
    {
        if (centralWidget() != choice)
        {
            choice_set();
            setCentralWidget(choice);
        }
    }

    void open()
    {

        if (centralWidget() != choice)
        {
            choice_set();
            setCentralWidget(choice);
        }
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "",
                                                        tr("Text Files (*.txt);;"));
        if (fileName != "")
        {
            QFile file(fileName);
            if (!file.open(QIODevice::ReadOnly))
            {
                QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
                return;
            }

            QString contents = file.readAll().constData();
            textEdit->setPlainText(contents);
            file.close();
        }
    }

    void text_ok()
    {
        QString tmp;
        tmp.setNum(spinbox->value());
        tmp = tmp + " " + textEdit->toPlainText();
        graph *mag = new graph(tmp);
        mag->coordinates();
        rendering = new drawing(this);
        rendering->setGraph(mag);
        rendering->show();
        setCentralWidget(rendering);
        textEdit->clear();
    }

    void generation()
    {
        srand(time(0));
        QString str;
        size_t n = rand() % 11 + 5;
        spinbox->setValue(n);
        for (size_t i = 0; i < n; ++i)
        {
            for (size_t j = 0; j < n; ++j)
            {
                int link = rand() % 100;
                if (link > 85 && i != j)
                    str += "1 ";
                else
                    str += "0 ";
            }
            str += "\n";
        }
        textEdit->clear();
        textEdit->setText(str);
    }

    void demo()
    {
        std::vector<std::vector<int> > tmp =
        {
            {0, 1, 0, 0, 1, 0},
            {1, 0, 1, 0, 1, 0},
            {0, 1, 0, 1, 0, 0},
            {0, 0, 1, 0, 1, 1},
            {1, 1, 0, 1, 0, 0},
            {1, 0, 0, 1, 0, 0}
        };

        graph *mag = new graph(tmp);
        mag->coordinates(true);
        rendering = new drawing(this);
        rendering->setGraph(mag);
        rendering->show();
        setCentralWidget(rendering);
        textEdit->clear();
    }

    void about()
    {
        QMessageBox::about(this, tr("О программе"),
                           tr("<p>Визуализация графов, v1.0.3 beta"
                              "<p>Автор: Винокурова Людмила"
                              ));
    }

public:

    void choice_set()
    {
        choice = new QWidget(this);
        spinbox = new QSpinBox(choice);
        slider = new QSlider(Qt::Horizontal, choice);
        spinbox->setRange(2, 25);
        slider->setRange(2, 25);
        connect(spinbox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
        connect(slider, SIGNAL(valueChanged(int)), spinbox, SLOT(setValue(int)));
        spinbox->setValue(7);

        layoutV = new QVBoxLayout(choice);
        layoutH = new QHBoxLayout;
        layoutS = new QHBoxLayout;
        layoutS->addWidget(spinbox);
        layoutS->addWidget(slider);

        textEdit = new QTextEdit(choice);
        quitButton1 = new QPushButton(tr("OK"), choice);
        quitButton2 = new QPushButton(tr("Случайный граф"), choice);
        quitButton3 = new QPushButton(tr("Демо режим"), choice);

        vertices = new QLabel(tr("Количество вершин в графе:"), choice);
        layoutV->addWidget(vertices);
        layoutV->addLayout(layoutS);

        matrix = new QLabel(tr("Матрица смежности:"), choice);

        layoutV->addWidget(matrix);
        layoutV->addWidget(textEdit);

        layoutH->addWidget(quitButton1);
        layoutH->addWidget(quitButton2);

        layoutH->addWidget(quitButton3);
        layoutV->addLayout(layoutH);
        choice->setLayout(layoutV);
        connect(quitButton1, SIGNAL(clicked()), this, SLOT(text_ok()));
        connect(quitButton2, SIGNAL(clicked()), this, SLOT(generation()));
        connect(quitButton3, SIGNAL(clicked()), this, SLOT(demo()));
    }

    myMainWidget (QMainWindow *parent = 0) : QMainWindow(parent)
    {
        setWindowTitle(tr("Визуализация графов"));

        choice_set();
        setCentralWidget(choice);

        newAction = new QAction(tr("Новый файл"), this);
        openAction = new QAction(tr("Открыть"), this);
        exitAction = new QAction(tr("Выход"), this);
        aboutAction = new QAction(tr("О программе"), this);

        connect(newAction, SIGNAL(triggered()), this, SLOT(newfile()));
        connect(openAction, SIGNAL(triggered()), this, SLOT(open()));
        connect(exitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
        connect(aboutAction, SIGNAL(triggered()), this, SLOT(about()));

        fileMenu = menuBar()->addMenu(tr("Файл"));
        fileMenu->addAction(newAction);
        fileMenu->addAction(openAction);
        fileMenu->addSeparator();
        fileMenu->addAction(exitAction);

        fileMenu = menuBar()->addMenu(tr("Помощь"));
        fileMenu->addAction(aboutAction);
    }

    QAction *newAction;
    QAction *openAction;
    QAction *exitAction;
    QAction *aboutAction;

    QMenu *fileMenu;
    drawing *rendering;
    QWidget *choice;
    QTextEdit *textEdit;

    QSpinBox *spinbox;
    QSlider *slider;

    QHBoxLayout *layoutS;
    QPushButton *quitButton1;
    QPushButton *quitButton2;
    QPushButton *quitButton3;
    QVBoxLayout *layoutV;
    QHBoxLayout *layoutH;
    QLabel *vertices;
    QLabel *matrix;
};

#include "main.moc"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    myMainWidget window;
    int x = std::min(app.desktop()->width(), app.desktop()->height()) / 2;
    window.resize(x, x);
    window.show();
    return app.exec();
}
